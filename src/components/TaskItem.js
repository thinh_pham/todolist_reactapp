import React from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskList extends React.Component {

  onUpdateTask = () => {
    this.props.onUpdateTask(this.props.task);
    this.props.openForm();
  }

  onDeleteTask = () => {
    this.props.onDeleteTask(this.props.task.id);
  }

  render() {
    var { task, index } = this.props;
    return (
      <tr>
        <th scope="row">{ index + 1 }</th>
        <td>{ task.time }h</td>
        <td>{ task.name }</td>
        <td>
          { task.level === -1 ? 'Low' : (task.level === 0 ? 'Medium' : 'High') }
        </td>
        <td>{ task.status ? 'Done' : 'Not Done' }</td>
        <td>
          <button
            type="button"
            className="btn btn-outline-warning btn-sm"
            onClick={ this.onUpdateTask }
          >
            <span className="fas fa-pencil-alt mr-2" />Edit
          </button>
          &nbsp;
          <button
            type="button"
            className="btn btn-outline-danger btn-sm"
            onClick={ this.onDeleteTask }
          >
            <span className="fas fa-trash-alt mr-2" />Delete
          </button>
        </td>
      </tr>
    );
  };
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateTask : (task) => {
      dispatch(actions.editTask(task));
    },
    onDeleteTask : (id) => {
      dispatch(actions.deleteTask(id));
    },
    openForm : () => {
      dispatch(actions.openForm());
    }
  }
};

export default connect(null, mapDispatchToProps)(TaskList);
