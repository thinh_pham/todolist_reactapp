import React from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id : '',
      time : 0,
      name : '',
      level : -1,
      status : true
    };
  }

  UNSAFE_componentWillMount() {
    var { taskEditting } = this.props;
    if(taskEditting && taskEditting.id !== '') {
      this.setState({
        id : taskEditting.id,
        time : taskEditting.time,
        name : taskEditting.name,
        level : parseInt(taskEditting.level, 10),
        status : taskEditting.status
      });
    } else {
      this.onClear();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    var { taskEditting } = nextProps;
    if(taskEditting && taskEditting.id !== '') {
      this.setState({
        id : taskEditting.id,
        time : taskEditting.time,
        name : taskEditting.name,
        level : parseInt(taskEditting.level, 10),
        status : taskEditting.status
      });
    } else {
      this.onClear();
    }
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSave(this.state);
    this.onCloseForm();
  }

  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name] : value
    });
  }

  onClear = () => {
    this.setState({
      id : '',
      time : 0,
      name : '',
      level : -1,
      status : true
    });
  }

  onCloseForm = () => {
    this.props.onCloseForm();
    this.props.onEditTask({
      id : '',
      time : 0,
      name : '',
      level : -1,
      status : true
    });
  }

  render() {
    var { isDisplayForm } = this.props;

    if (!isDisplayForm) return '';

    return (
      <form onSubmit={ this.onSubmit }>
        <div className="form-row">
          <div className="col-2">
            <select
              className="custom-select custom-select-sm"
              name="time"
              value={ this.state.time }
              onChange={ this.onChange }
            >
              <option value={0}>0h</option>
              <option value={1}>1h</option>
              <option value={2}>2h</option>
              <option value={3}>3h</option>
              <option value={4}>4h</option>
              <option value={5}>5h</option>
              <option value={6}>6h</option>
              <option value={7}>7h</option>
              <option value={8}>8h</option>
              <option value={9}>9h</option>
              <option value={10}>10h</option>
              <option value={11}>11h</option>
              <option value={12}>12h</option>
              <option value={13}>13h</option>
              <option value={14}>14h</option>
              <option value={15}>15h</option>
              <option value={16}>16h</option>
              <option value={17}>17h</option>
              <option value={18}>18h</option>
              <option value={19}>19h</option>
              <option value={20}>20h</option>
              <option value={21}>21h</option>
              <option value={22}>22h</option>
              <option value={23}>23h</option>
            </select>
          </div>
          <div className="col-4">
            <input
              type="text"
              className="form-control form-control-sm"
              placeholder="Name"
              name="name"
              value={ this.state.name }
              onChange={ this.onChange }
              autoComplete="off"
            />
          </div>
          <div className="col-2">
            <select
              className="custom-select custom-select-sm"
              name="level"
              value={ this.state.level }
              onChange={ this.onChange }
            >
              <option value={-1}>Low</option>
              <option value={0}>Medium</option>
              <option value={1}>High</option>
            </select>
          </div>
          <div className="col-2">
            <select
              className="custom-select custom-select-sm"
              name="status"
              value={ this.state.status }
              onChange={ this.onChange }
            >
              <option value={true}>Done</option>
              <option value={false}>Not Done</option>
            </select>
          </div>
          <div className="col-2">
            <button type="submit" className="btn btn-outline-success btn-sm mr-10">
              Save<span className="fas fa-save ml-2" />
            </button>
            &nbsp;
            <button
              type="reset"
              className="btn btn-outline-warning btn-sm"
              onClick={ this.onCloseForm }
            >
              Cancel<span className="fas fa-ban ml-2" />
            </button>
          </div>
        </div>
      </form>
    );
  };
}

const mapStateToProps = state => {
  return {
    isDisplayForm : state.isDisplayForm,
    taskEditting : state.taskEditting
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSave : (task) => {
      dispatch(actions.saveTask(task));
    },
    onEditTask : (task) => {
      dispatch(actions.editTask(task));
    },
    onCloseForm : () => {
      dispatch(actions.closeForm());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);
