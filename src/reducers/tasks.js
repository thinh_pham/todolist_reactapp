import * as types from './../constants/ActionTypes';
import { findIndex } from 'lodash';

var s4 = () => {
	return Math.floor((1 + Math.random()) * 0x10000).toString(10).substring(1);
}

var generateID = () => {
	return s4() + '-' + s4() + '--' + s4() + '-' + s4();
}

var data = JSON.parse(localStorage.getItem('tasks'));

var initialState = data ? data : [];

var myReducer = (state = initialState, action) => {
	var index = -1;
	switch(action.type) {
		case types.SAVE_TASK:
			var task = {
				id : action.task.id,
				time : parseInt(action.task.time, 10),
				name : action.task.name,
				level : parseInt(action.task.level, 10),
				status : (action.task.status === true || action.task.status === 'true') ? true : false
			}

			if(task.id !== '') {
				index = findIndex(state, (task) => { return task.id === action.task.id });
				state[index] = task;
			} else {
				task.id = generateID();
				// state.push(task);
				state = [...state, task];
			}
			localStorage.setItem('tasks', JSON.stringify(state));

			return [...state];

		case types.DELETE_TASK:
			index = findIndex(state, (task) => { return task.id === action.id })
			if(index !== -1) {
				state.splice(index, 1);
				localStorage.setItem('tasks', JSON.stringify(state));
			}
			return [...state];

		default: return state;
	}
}

export default myReducer;