import React from 'react';
import TaskSort from './TaskSort';
import TaskSearch from './TaskSearch';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskControl extends React.Component {

  onOpenForm = () => {
    this.props.openForm();
  }

  render() {
    var { isDisplayForm, taskEditting } = this.props;

    return (
      <div className="form-row">
        <div className="col-2">
          <button
          type="button"
          className="btn btn-outline-info"
          onClick={ this.onOpenForm }
          disabled={ isDisplayForm ? 'disabled' : '' }
          >
            <span className="fas fa-plus mr-2"></span>
            { (taskEditting && taskEditting.id !== '') ? 'Edit Task' : 'Add Task' }
          </button>
        </div>
        <div className="col-2">
          <TaskSort />
        </div>
        <div className="col-8">
          <TaskSearch />
        </div>
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    isDisplayForm : state.isDisplayForm,
    taskEditting : state.taskEditting
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    openForm : () => {
      dispatch(actions.openForm());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskControl);
