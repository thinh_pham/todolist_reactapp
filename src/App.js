import React from 'react';
import './App.css';
import TaskControl from './components/TaskControl';
import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row mt-2">
          <div className="col">
            <div className="card text-center">
              <div className="cart-header pt-3">
                <h1 className="text-uppercase text-monospace">~ todo list ~</h1>
                <hr />
              </div>
              <div className="card-body">                
                <div className="row">
                  <div className="col-12">
                    <TaskControl />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-12">
                    <TaskForm />
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-12">
                    <TaskList />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default App;