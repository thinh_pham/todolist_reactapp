import { combineReducers } from 'redux';
import tasks from './tasks';
import isDisplayForm from './isDisplayForm';
import taskEditting from './taskEditting';
import sort from './sort';
import search from './search';

var myReducer = combineReducers({
	tasks,
	isDisplayForm,
	taskEditting,
	sort,
	search
});

export default myReducer;