import * as types from './../constants/ActionTypes';

var initialState = {
	id : '',
	time : 0,
  name : '',
  level : 'Low',
  status : true
};

var myReducer = (state = initialState, action) => {
	switch(action.type) {
		case types.EDIT_TASK:
			return action.task;

		default: return state;
	}
}

export default myReducer;