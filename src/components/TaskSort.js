import React from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskSort extends React.Component {

  onSort = (sortBy, sortValue) => {
    this.props.onSort({
      by : sortBy,
      value : sortValue
    });
  }

  render() {
    var { sort } = this.props;

    return (
      <div className="dropdown">
        <button
          className="btn btn-outline-primary dropdown-toggle"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          Sort By
        </button>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('time', 1) }
          >
            Time ASC
            <i className={ sort.by === 'time' && sort.value === 1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'time' && sort.value === 1 ? 'check' : '' }
            </i>
          </a>
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('time', -1) }
          >
            Time DESC
            <i className={ sort.by === 'time' && sort.value === -1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'time' && sort.value === -1 ? 'check' : '' }
            </i>
          </a>
          <div className="dropdown-divider" />
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('status', 1) }
          >
            Done
            <i className={ sort.by === 'status' && sort.value === 1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'status' && sort.value === 1 ? 'check' : '' }
            </i>
          </a>
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('status', -1) }
          >
            Not Done
            <i className={ sort.by === 'status' && sort.value === -1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'status' && sort.value === -1 ? 'check' : '' }
            </i>
          </a>
          <div className="dropdown-divider" />
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('name', 1) }
          >
            Name ASC
            <i className={ sort.by === 'name' && sort.value === 1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'name' && sort.value === 1 ? 'check' : '' }
            </i>
          </a>
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('name', -1) }
          >
            Name DESC
            <i className={ sort.by === 'name' && sort.value === -1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'name' && sort.value === -1 ? 'check' : '' }
            </i>
          </a>
          <div className="dropdown-divider" />
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('level', 1) }
          >
            Level ASC
            <i className={ sort.by === 'level' && sort.value === 1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'level' && sort.value === 1 ? 'check' : '' }
            </i>
          </a>
          <a
            className="dropdown-item"
            onClick={ () => this.onSort('level', -1) }
          >
            Level DESC
            <i className={ sort.by === 'level' && sort.value === -1 ? 'material-icons md-18 ml-4' : '' }>
              { sort.by === 'level' && sort.value === -1 ? 'check' : '' }
            </i>
          </a>
        </div>
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    sort : state.sort
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSort : (sort) => {
      dispatch(actions.sort(sort));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskSort);