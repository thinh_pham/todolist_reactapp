import React from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskSearch extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      keyword : ''
    };
  }

  onChange = (event) => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name] : value
    });
  }

  onSearch = () => {
    this.props.onSearch(this.state);
  }

  onClear = () => {
    this.props.onEditTask({
      id : this.props.taskEditting.id,
      time : 0,
      name : '',
      level : -1,
      status : true
    });
    this.setState({ keyword : '' });
  }

  render() {
    return (
      <div className="row">
        <div className="col-8">
          <input
            type="text"
            className="form-control"
            placeholder="Search"
            name="keyword"
            value={ this.state.keyword }
            onChange={ this.onChange }
            autoComplete="off"
          />
        </div>
        <div className="col-4">
          <button
            type="button"
            className="btn btn-outline-success"
            onClick={ this.onSearch }
          >
            <span className="fas fa-search mr-1" />Search
          </button>
          &nbsp;
          <button
            type="button"
            className="btn btn-outline-warning"
            onClick={ this.onClear }
          >
            <span className="fas fa-trash-restore mr-1" />Clear
          </button>
        </div>
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    taskEditting : state.taskEditting
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch : (keyword) => {
      dispatch(actions.search(keyword));
    },
    onEditTask : (task) => {
      dispatch(actions.editTask(task));
    },
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskSearch);
