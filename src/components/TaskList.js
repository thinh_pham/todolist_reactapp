import React from 'react';
import TaskItem from './TaskItem';
import { connect } from 'react-redux';
import { indexOf } from 'lodash';

class TaskList extends React.Component {
  render() {
    
    var { tasks, keyword, sort } = this.props;

    // Search
    if(keyword) {
      tasks = tasks.filter(task => {
        return indexOf(task.name.toLowerCase(), keyword) !== -1;
      });
      // tasks = indexOf(tasks, keyword) !== -1 ? tasks[indexOf(tasks, keyword)] : [];
      // var index = indexOf(tasks, keyword);
      // console.log(index);
    }

    // Sort
    if(sort.by === 'time') {
      tasks.sort((a, b) => {
        if(a.time > b.time) return sort.value;
        else if(a.time < b.time) return -sort.value;
        return 0;
      });
    }

    if(sort.by === 'status') {
      tasks.sort((a, b) => {
        if(a.status > b.status) return -sort.value;
        else if(a.status < b.status) return sort.value;
        return 0;
      });
    }

    if(sort.by === 'name') {
      tasks.sort((a, b) => {
        if(a.name > b.name) return sort.value;
        else if(a.name < b.name) return -sort.value;
        return 0;
      });
    }

    if(sort.by === 'level') {
      tasks.sort((a, b) => {
        if(a.level > b.level) return sort.value;
        else if(a.level < b.level) return -sort.value;
        return 0;
      });
    }


    var elmTasks = tasks.map((task, index) => {
      return <TaskItem key={ task.id } index={ index } task={ task } />
    })

    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Time</th>
            <th scope="col">Name</th>
            <th scope="col">Level</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        { elmTasks }
        </tbody>
      </table>
    );
  };
}

const mapStateToProps = state => {
  return {
    tasks : state.tasks,
    sort : state.sort,
    keyword : state.search
  }
};

export default connect(mapStateToProps, null)(TaskList);
